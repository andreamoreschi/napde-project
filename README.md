# NAPDE course project A.Y. 2020/2021

**Authors:** Mazzilli Giulia, Moreschi Andrea, Viti Bruno.

*Code tested on Windows 10 version 21H1 and Ubuntu 16.04.4, [FreeFem++](https://freefem.org/) and [MATLAB](https://mathworks.com/) need to be installed*

**Launch a .edp script in Windows**: Click on it.

**Launch a .edp script in Ubuntu**: 

1. `cd path/to/the/script`
2. `FreeFem++ main.edp`
				    

------------------------------------------

# First_Method folder
* **main.edp**: main script which plots the mesh, the first guess of the temperature, the exact and the numerical velocity field. It generates the .msh and .txt files with the data needed by the MATLAB script.

* **temperature_pression_plots_1.m**: Employs the .msh and .txt files generated by main.edp file to plot the mesh, the exact and numerical temperature and pressure.

* **Results folder**: Folder in which are saved all the .msh and .txt files generated by main.edp.

* **Convergence test folder**:
  * **convergence_test.edp**: Performs a convergence test with different mesh refinements exporting the .dat files with the errors and the mesh sizes used by the MATLAB script.

  * **convergence_test_mat_1.m**: Employs the .dat files generated to see graphically the convergence rate in MATLAB.


# Second_Method folder
* **main.edp**: main script which plots the mesh, the first guess of the temperature, the exact and the numerical velocity field. It generates the .msh and .txt files with the data needed by the MATLAB script.

* **temperature_pression_plots_2.m**: Employs the .msh and .txt files generated by main.edp file to plot the mesh, the exact and numerical temperature and pressure.

* **Results folder**: Folder in which are saved all the .msh and .txt files generated by main.edp.

* **Convergence test folder**:
  * **convergence_test.edp**: Performs a convergence test with different mesh refinements exporting the .dat files with the errors and the mesh sizes used by the MATLAB scripts.

  * **convergence_test_mat_2**: Employs the .dat files generated to see graphically the convergence rate in MATLAB.


# Utilities folder
* **utilities.idp**: Various functions used many times in the .edp scripts.

* **ffmatlib.idp**: External library used to export the results in order to be able to manipulate them in MATLAB.

* **MATLAB scripts (.m)**: Employ the files generated by the FreeFem code to plot the numerical fields.
