/**
  * Authors: Mazzilli Giulia, Moreschi Andrea, Viti Bruno
  * June 2021
  *
  * This script solves by successive approximations the Darcy equation coupled
  * with the Heat equation following the second method proposed in:
  *
  * Bernardi, C., Dib, S., Girault, V. et al. Finite element methods for
  * Darcy’s problem coupled with the heat equation.
  * Numer. Math. 139, 315–348 (2018). https://doi.org/10.1007/s00211-017-0938-y
  *
**/

include "../Utilities/ffmatlib.idp"
include "../Utilities/utilities.idp"


//---------------------PARAMETERS-------------------------


//Domain size [0, L] x [0, L]
int L = 3;

//Discretization along the axis to build the mesh
int NN = 50;

//Max number of the iterations of the Successive Approximations method
int MaxIterations = 100;

//Tolerance to stop the Successive Approximations method
real tolerance = 1e-5;

//Diffusion coefficient
real alpha = 3;

//Solution coefficient
real beta = 5;

//Exact Stream function
func PsiEx = exp(-beta * ((x - 1)^2 + (y - 1)^2));

//Exact Velocity field [dy(PsiEx) -dx(PsiEx)]
func uEx = [-beta*exp(-beta*((x - 1)^2 + (y - 1)^2))*(2*y - 2),
            beta*exp(-beta*((x - 1)^2 + (y - 1)^2))*(2*x - 2)];

//Exact gradient of the x-component of the Velocity field
func GradU1 = [beta^2*exp(-beta*((x - 1)^2 + (y - 1)^2))*(2*x - 2)*(2*y - 2),
               beta^2*exp(-beta*((x - 1)^2 + (y - 1)^2))*(2*y - 2)^2 - 2*beta*exp(-beta*((x - 1)^2 + (y - 1)^2))];

//Exact gradient of the y-component of the Velocity field
func GradU2 = [2*beta*exp(-beta*((x - 1)^2 + (y - 1)^2)) - beta^2*exp(-beta*((x - 1)^2 + (y - 1)^2))*(2*x - 2)^2,
               -beta^2*exp(-beta*((x - 1)^2 + (y - 1)^2))*(2*x - 2)*(2*y - 2)];

//Exact Pressure
func PEx = cos((pi * x) / 3) * cos((pi * y) / 3);

//Exact Temperature
func TEx = x^2 * (x - 3)^2 * y^2 * (y-3)^2;

//Exact gradient of the Temperature
func GradT = [2*x*y^2*(x - 3)^2*(y - 3)^2 + x^2*y^2*(2*x - 6)*(y - 3)^2,
              2*x^2*y*(x - 3)^2*(y - 3)^2 + x^2*y^2*(2*y - 6)*(x - 3)^2];

//Exact viscosity
func nu = TEx + 1;

//External density force for the Darcy equation
func f = [- (pi*cos((pi*y)/3)*sin((pi*x)/3))/3 - beta*nu*exp(-beta*((x - 1)^2 + (y - 1)^2))*(2*y - 2),
          beta*nu*exp(-beta*((x - 1)^2 + (y - 1)^2))*(2*x - 2) - (pi*cos((pi*x)/3)*sin((pi*y)/3))/3];

//External heat source for the Heat equation
func g = beta*exp(-beta*((x - 1)^2 + (y - 1)^2))*(2*x - 2)*(2*x^2*y*(x - 3)^2*(y - 3)^2 + x^2*y^2*(2*y - 6)*(x - 3)^2) - alpha*(2*x^2*y^2*(x - 3)^2 + 2*x^2*y^2*(y - 3)^2 + 2*x^2*(x - 3)^2*(y - 3)^2 + 2*y^2*(x - 3)^2*(y - 3)^2 + 4*x*y^2*(2*x - 6)*(y - 3)^2 + 4*x^2*y*(2*y - 6)*(x - 3)^2) - beta*exp(-beta*((x - 1)^2 + (y - 1)^2))*(2*y - 2)*(2*x*y^2*(x - 3)^2*(y - 3)^2 + x^2*y^2*(2*x - 6)*(y - 3)^2);

//Penalty coefficient to force the b.c. (It Has to be a big number)
real PenCoeff = 10^30;


//---------------------SOLUTION OF THE PROBLEM-------------------------


//Building and plot of the Mesh
mesh Th = CreateMesh(NN, L);

//Definition of the FE spaces, solutions and test functions
fespace Xh(Th, P1);   // Temperature
fespace Wh(Th, P1b);  // Velocity
fespace Mh(Th, P1);   // Pressure
Xh T, TOld, S, nuDisc;
Wh u1, u2, u1Old, u2Old, v1, v2;
Mh p, pOld, q;

//Initial Guess of the Temperature for the Successive Approximations method
SolveTemperatureGuess (T, S, alpha, g)

plot(T, value = true, cmm = "First guess of Temperature");


//Solution of the coupled problem with Successive Approximations
int NumIt = 1;
real  error = tolerance + 1;

while(NumIt < MaxIterations && error > tolerance)
{
  cout << "============================================================" << endl;
  cout << "Iteration " << NumIt << " of " << MaxIterations << endl;
  cout << "============================================================" << endl;

  //Update of the viscosity
  nuDisc = T + 1;

  if (NumIt >= 2)
  {
    TOld = T;
    u1Old = u1;
    u2Old = u2;
    pOld = p;
  }

  //Darcy equation
  SolveDarcySecond (u1, u2, p, v1, v2, q, nu, PenCoeff, f)

  //Heat equation
  SolveHeatSecond (T, S, alpha, g, u1, u2)

  //Computation of the error ||u-uOld||L2 + ||p-pOld||L2 + |T-TOld|H1 between two successive iterations
  if (NumIt >= 2)
  {
      error = sqrt(int2d(Th)((u1-u1Old)^2 + (u2-u2Old)^2)) +
              sqrt(int2d(Th)((p-pOld)^2)) +
              sqrt(int2d(Th)((dx(T)-dx(TOld))^2 + (dy(T)-dy(TOld))^2));

      cout << "##### Error = " << error << " #####"<< endl;

  }

    ++NumIt;
}

if (NumIt == MaxIterations)
{
  cout << "============================================================" << endl;
  cout << "Maximum Number of iterations reached" << endl;
  cout << "============================================================" << endl;
}


//--------------------PLOT AND EXPORT OF THE DATA-------------------------


//Plot of the numerical Velocity field. To see it press "p" when visualizing the exact solution in the opening window
plot([u1, u2], value = true, cmm = "Velocity");

//Plot of the projection over the space Wh of the exact Velocity field
Wh uEx1 = uEx[0], uEx2 = uEx[1];
plot([uEx1, uEx2], value = true, cmm = "Exact Velocity");

//Export of the Mesh, Temperature and Pression solutions in order to be plotted in MATLAB
savemesh(Th, "Results/Mesh.msh");
ffSaveVh(Th, Xh, "Results/Temperature_Space.txt");
ffSaveVh(Th, Mh, "Results/Pressure_Space.txt");
ffSaveData(p, "Results/Pressure.txt");
ffSaveData(T, "Results/Temperature.txt");

cout << "============================================================" << endl;
cout << "End of the program" << endl;
cout << "============================================================" << endl;
