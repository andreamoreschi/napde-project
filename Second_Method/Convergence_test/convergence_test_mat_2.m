% Authors: Mazzilli Giulia, Moreschi Andrea, Viti Bruno
% June 2021
%
% Convergence test for the second method presented in:
%
% Bernardi, C., Dib, S., Girault, V. et al. Finite element methods for
% Darcy’s problem coupled with the heat equation.
% Numer. Math. 139, 315–348 (2018). https://doi.org/10.1007/s00211-017-0938-y

clear
close all
clc

error = importdata('Error.dat');
h = importdata('Mesh_Size.dat');

N_tests = length(error);

desired_error = 1; % 1 -> total error
                   % 2 -> velocity error
                   % 3 -> pressure error
                   % 4 -> temperature error
                   
error = error(:, desired_error);
rate=mean(log10(error(2:N_tests)./error(1:N_tests-1))./log10(h(2:N_tests)./h(1:N_tests-1)));

figure()
loglog(h,error, 'or-', 'Linewidth', 2);
hold on
loglog(h, error(1)/h(1)*h, 'k--', 'Linewidth', 2);

legend ("Error", "h", 'Location', 'best', 'Interpreter', 'latex');
ylabel('Error', 'Interpreter', 'latex');
xlabel('h', 'Interpreter', 'latex');