% Authors: Mazzilli Giulia, Moreschi Andrea, Viti Bruno
% June 2021
%
% Plot of the Temperature and Pressure obtained with the first method
% presented in: 
%
% Bernardi, C., Dib, S., Girault, V. et al. Finite element methods for
% Darcy’s problem coupled with the heat equation.
% Numer. Math. 139, 315–348 (2018). https://doi.org/10.1007/s00211-017-0938-y
 
clear
close all
clc

addpath('../Utilities');

[p,b,t,nv,nbe,nt,labels]=ffreadmesh('Results/Mesh.msh');

[Xh]=ffreaddata('Results/Temperature_Space.txt');
[Mh]=ffreaddata('Results/Pressure_Space.txt');

[T]=ffreaddata('Results/Temperature.txt');
T_Ex = @(x,y) x.^2 .* (x - 3).^2 .* y.^2 .* (y-3).^2;

[P]=ffreaddata('Results/Pressure.txt');
P_Ex = @(x,y) cos((pi * x) / 3) .* cos((pi * y) / 3);


% Mesh Plot

figure;

ffpdeplot(p,b,t, ...
          'Mesh','on', ...
          'Boundary','on', ...
          'Title','Boundary/Edge and 2D Mesh');

ylabel('y');
xlabel('x');




% Temperature

figure;

ffpdeplot(p,b,t, ...
          'VhSeq',Xh, ...
          'XYData',T, ...
          'ZStyle','continuous', ...
          'Mesh','on', ...
          'Colormap', parula, ...
          'Title','Numerical Temperature');
ylabel('y');
xlabel('x');
zlabel('T');
grid;
axis tight square;

% Exact Temperature

figure;

[X,Y] = meshgrid(0:0.01:3,0:0.01:3);
s = surf(X,Y,T_Ex(X,Y));
s.EdgeColor = 'none';
title('Exact Temperature');
zlim([min(T), max(T)]);
ylabel('y');
xlabel('x');
zlabel('T');
axis tight square;
colorbar;
colormap parula;




% Pressure

figure;

ffpdeplot(p,b,t, ...
          'VhSeq',Mh, ...
          'XYData',P, ...
          'ZStyle','continuous', ...
          'Mesh','on', ...
          'Colormap', parula, ...
          'Title','Numerical Pressure');
ylabel('y');
xlabel('x');
zlabel('P');
grid;
axis tight square;

% Exact Pressure

figure;

[X,Y] = meshgrid(0:0.01:3,0:0.01:3);
s = surf(X,Y,P_Ex(X,Y));
s.EdgeColor = 'none';
title('Exact Pressure');
zlim([min(P), max(P)]);
ylabel('y');
xlabel('x');
zlabel('P');
axis tight square;
colorbar;
colormap parula;
